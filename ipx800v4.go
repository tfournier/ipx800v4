package ipx800v4

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

const timeout = time.Second * 5

var (
	ErrInvalidRequest = errors.New("invalid request")
	ErrInvalidBody    = errors.New("invalid body")
)

type IPX800V4 struct {
	url string
	log *log.Logger
}

func New(address string, apiKey interface{}) *IPX800V4 {
	ipx := &IPX800V4{
		url: fmt.Sprintf("http://%s/api/xdevices.json?", address),
		log: log.New(ioutil.Discard, "[IPX800V4] ", log.Ldate|log.Ltime|log.Lmsgprefix),
	}

	if apiKey != nil {
		ipx.url += fmt.Sprintf("key=%v", apiKey)
	}

	return ipx
}

func (ipx *IPX800V4) SetDebug() {
	ipx.log.SetOutput(os.Stdout)
}

func (ipx IPX800V4) Request(cmds Commands) (*Response, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), timeout)
	defer cancel()

	ipx.log.Printf("CMDS: %#v", cmds)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, ipx.url+cmds.ToString(), nil)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	ret, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	defer ret.Body.Close()

	if http.StatusBadRequest <= ret.StatusCode {
		return nil, ErrInvalidRequest
	}

	ipx.log.Printf("New request: %s", ret.Request.URL.String())

	res := Response{}

	if body, err := ioutil.ReadAll(ret.Body); err == nil {
		ipx.log.Printf("Response: %s", string(body))

		if err := json.Unmarshal(body, &res); err == nil {
			if res.Status == "Error" {
				return nil, ErrInvalidBody
			}
		}
	}

	return &res, nil
}

func (ipx IPX800V4) All() All {
	return All{IPX800V4: ipx}
}

func (ipx IPX800V4) Relays() Relays {
	return Relays{IPX800V4: ipx}
}
