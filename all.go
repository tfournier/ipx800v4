package ipx800v4

type All struct {
	IPX800V4
}

func (a All) Get() (*Response, error) {
	return a.Request(Commands{"Get": "all"})
}
