package ipx800v4

type (
	// Response is global API response object
	Response struct {
		Product string `json:"product"`
		Status  string `json:"status"`

		R1  *int `json:"R1,omitempty"`
		R2  *int `json:"R2,omitempty"`
		R3  *int `json:"R3,omitempty"`
		R4  *int `json:"R4,omitempty"`
		R5  *int `json:"R5,omitempty"`
		R6  *int `json:"R6,omitempty"`
		R7  *int `json:"R7,omitempty"`
		R8  *int `json:"R8,omitempty"`
		R9  *int `json:"R9,omitempty"`
		R10 *int `json:"R10,omitempty"`
		R11 *int `json:"R11,omitempty"`
		R12 *int `json:"R12,omitempty"`
		R13 *int `json:"R13,omitempty"`
		R14 *int `json:"R14,omitempty"`
		R15 *int `json:"R15,omitempty"`
		R16 *int `json:"R16,omitempty"`
		R17 *int `json:"R17,omitempty"`
		R18 *int `json:"R18,omitempty"`
		R19 *int `json:"R19,omitempty"`
		R20 *int `json:"R20,omitempty"`
		R21 *int `json:"R21,omitempty"`
		R22 *int `json:"R22,omitempty"`
		R23 *int `json:"R23,omitempty"`
		R24 *int `json:"R24,omitempty"`
		R25 *int `json:"R25,omitempty"`
		R26 *int `json:"R26,omitempty"`
		R27 *int `json:"R27,omitempty"`
		R28 *int `json:"R28,omitempty"`
		R29 *int `json:"R29,omitempty"`
		R30 *int `json:"R30,omitempty"`
		R31 *int `json:"R31,omitempty"`
		R32 *int `json:"R32,omitempty"`
		R33 *int `json:"R33,omitempty"`
		R34 *int `json:"R34,omitempty"`
		R35 *int `json:"R35,omitempty"`
		R36 *int `json:"R36,omitempty"`
		R37 *int `json:"R37,omitempty"`
		R38 *int `json:"R38,omitempty"`
		R39 *int `json:"R39,omitempty"`
		R40 *int `json:"R40,omitempty"`
		R41 *int `json:"R41,omitempty"`
		R42 *int `json:"R42,omitempty"`
		R43 *int `json:"R43,omitempty"`
		R44 *int `json:"R44,omitempty"`
		R45 *int `json:"R45,omitempty"`
		R46 *int `json:"R46,omitempty"`
		R47 *int `json:"R47,omitempty"`
		R48 *int `json:"R48,omitempty"`
		R49 *int `json:"R49,omitempty"`
		R50 *int `json:"R50,omitempty"`
		R51 *int `json:"R51,omitempty"`
		R52 *int `json:"R52,omitempty"`
		R53 *int `json:"R53,omitempty"`
		R54 *int `json:"R54,omitempty"`
		R55 *int `json:"R55,omitempty"`
		R56 *int `json:"R56,omitempty"`
	}
)
