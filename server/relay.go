package server

import (
	"net/http"
	"strconv"
)

// RelayResponse return methods object
type RelayResponse struct {
	Response
	R1  int `json:"R1"`
	R2  int `json:"R2"`
	R3  int `json:"R3"`
	R4  int `json:"R4"`
	R5  int `json:"R5"`
	R6  int `json:"R6"`
	R7  int `json:"R7"`
	R8  int `json:"R8"`
	R9  int `json:"R9"`
	R10 int `json:"R10"`
	R11 int `json:"R11"`
	R12 int `json:"R12"`
	R13 int `json:"R13"`
	R14 int `json:"R14"`
	R15 int `json:"R15"`
	R16 int `json:"R16"`
	R17 int `json:"R17"`
	R18 int `json:"R18"`
	R19 int `json:"R19"`
	R20 int `json:"R20"`
	R21 int `json:"R21"`
	R22 int `json:"R22"`
	R23 int `json:"R23"`
	R24 int `json:"R24"`
	R25 int `json:"R25"`
	R26 int `json:"R26"`
	R27 int `json:"R27"`
	R28 int `json:"R28"`
	R29 int `json:"R29"`
	R30 int `json:"R30"`
	R31 int `json:"R31"`
	R32 int `json:"R32"`
	R33 int `json:"R33"`
	R34 int `json:"R34"`
	R35 int `json:"R35"`
	R36 int `json:"R36"`
	R37 int `json:"R37"`
	R38 int `json:"R38"`
	R39 int `json:"R39"`
	R40 int `json:"R40"`
	R41 int `json:"R41"`
	R42 int `json:"R42"`
	R43 int `json:"R43"`
	R44 int `json:"R44"`
	R45 int `json:"R45"`
	R46 int `json:"R46"`
	R47 int `json:"R47"`
	R48 int `json:"R48"`
	R49 int `json:"R49"`
	R50 int `json:"R50"`
	R51 int `json:"R51"`
	R52 int `json:"R52"`
	R53 int `json:"R53"`
	R54 int `json:"R54"`
	R55 int `json:"R55"`
	R56 int `json:"R56"`
}

func getR(c *context) error {
	return c.JSONPretty(http.StatusOK, RelayResponse{
		Response: c.ResponseSuccess(),
		R1:       c.Relays[1],
		R2:       c.Relays[2],
		R3:       c.Relays[3],
		R4:       c.Relays[4],
		R5:       c.Relays[5],
		R6:       c.Relays[6],
		R7:       c.Relays[7],
		R8:       c.Relays[8],
		R9:       c.Relays[9],
		R10:      c.Relays[10],
		R11:      c.Relays[11],
		R12:      c.Relays[12],
		R13:      c.Relays[13],
		R14:      c.Relays[14],
		R15:      c.Relays[15],
		R16:      c.Relays[16],
		R17:      c.Relays[17],
		R18:      c.Relays[18],
		R19:      c.Relays[19],
		R20:      c.Relays[20],
		R21:      c.Relays[21],
		R22:      c.Relays[22],
		R23:      c.Relays[23],
		R24:      c.Relays[24],
		R25:      c.Relays[25],
		R26:      c.Relays[26],
		R27:      c.Relays[27],
		R28:      c.Relays[28],
		R29:      c.Relays[29],
		R30:      c.Relays[30],
		R31:      c.Relays[31],
		R32:      c.Relays[32],
		R33:      c.Relays[33],
		R34:      c.Relays[34],
		R35:      c.Relays[35],
		R36:      c.Relays[36],
		R37:      c.Relays[37],
		R38:      c.Relays[38],
		R39:      c.Relays[39],
		R40:      c.Relays[40],
		R41:      c.Relays[41],
		R42:      c.Relays[42],
		R43:      c.Relays[43],
		R44:      c.Relays[44],
		R45:      c.Relays[45],
		R46:      c.Relays[46],
		R47:      c.Relays[47],
		R48:      c.Relays[48],
		R49:      c.Relays[49],
		R50:      c.Relays[50],
		R51:      c.Relays[51],
		R52:      c.Relays[52],
		R53:      c.Relays[53],
		R54:      c.Relays[54],
		R55:      c.Relays[55],
		R56:      c.Relays[56],
	}, indent)
}

func setR(c *context) error {
	id, err := strconv.Atoi(c.QueryParam("SetR"))
	if err != nil {
		return c.JSONPretty(http.StatusOK, c.ResponseError(), indent)
	}

	if !(1 <= id && id <= 56) {
		return c.JSONPretty(http.StatusOK, c.ResponseError(), indent)
	}

	c.Relays[id] = 1

	return c.JSONPretty(http.StatusOK, c.ResponseSuccess(), indent)
}

func clearR(c *context) error {
	id, err := strconv.Atoi(c.QueryParam("ClearR"))
	if err != nil {
		return c.JSONPretty(http.StatusOK, c.ResponseError(), indent)
	}

	if !(1 <= id && id <= 56) {
		return c.JSONPretty(http.StatusOK, c.ResponseError(), indent)
	}

	c.Relays[id] = 0

	return c.JSONPretty(http.StatusOK, c.ResponseSuccess(), indent)
}

func toggleR(c *context) error {
	id, err := strconv.Atoi(c.QueryParam("ToggleR"))
	if err != nil {
		return c.JSONPretty(http.StatusOK, c.ResponseError(), indent)
	}

	if !(1 <= id && id <= 56) {
		return c.JSONPretty(http.StatusOK, c.ResponseError(), indent)
	}

	if c.Relays[id] == 0 {
		c.Relays[id] = 1
	} else {
		c.Relays[id] = 0
	}

	return c.JSONPretty(http.StatusOK, c.ResponseSuccess(), indent)
}
