package server

// Response is object response from API
type Response struct {
	Product string `json:"product"`
	Status  string `json:"status"`
}
