package server

import "net/http"

// AllResponse return methods object
type AllResponse struct {
	Response
	RelayResponse
}

func getAll(c *context) error {
	return c.JSONPretty(http.StatusOK, AllResponse{
		Response: c.ResponseSuccess(),
		RelayResponse: RelayResponse{
			R1:  c.Relays[1],
			R2:  c.Relays[2],
			R3:  c.Relays[3],
			R4:  c.Relays[4],
			R5:  c.Relays[5],
			R6:  c.Relays[6],
			R7:  c.Relays[7],
			R8:  c.Relays[8],
			R9:  c.Relays[9],
			R10: c.Relays[10],
			R11: c.Relays[11],
			R12: c.Relays[12],
			R13: c.Relays[13],
			R14: c.Relays[14],
			R15: c.Relays[15],
			R16: c.Relays[16],
			R17: c.Relays[17],
			R18: c.Relays[18],
			R19: c.Relays[19],
			R20: c.Relays[20],
			R21: c.Relays[21],
			R22: c.Relays[22],
			R23: c.Relays[23],
			R24: c.Relays[24],
			R25: c.Relays[25],
			R26: c.Relays[26],
			R27: c.Relays[27],
			R28: c.Relays[28],
			R29: c.Relays[29],
			R30: c.Relays[30],
			R31: c.Relays[31],
			R32: c.Relays[32],
			R33: c.Relays[33],
			R34: c.Relays[34],
			R35: c.Relays[35],
			R36: c.Relays[36],
			R37: c.Relays[37],
			R38: c.Relays[38],
			R39: c.Relays[39],
			R40: c.Relays[40],
			R41: c.Relays[41],
			R42: c.Relays[42],
			R43: c.Relays[43],
			R44: c.Relays[44],
			R45: c.Relays[45],
			R46: c.Relays[46],
			R47: c.Relays[47],
			R48: c.Relays[48],
			R49: c.Relays[49],
			R50: c.Relays[50],
			R51: c.Relays[51],
			R52: c.Relays[52],
			R53: c.Relays[53],
			R54: c.Relays[54],
			R55: c.Relays[55],
			R56: c.Relays[56],
		},
	}, indent)
}
