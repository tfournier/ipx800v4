package server

import "github.com/labstack/echo/v4"

type context struct {
	echo.Context
	Relays map[int]int
}

func setContext() echo.MiddlewareFunc {
	relays := map[int]int{}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &context{
				Context: c,
				Relays:  relays,
			}

			return next(cc)
		}
	}
}

func (c context) ResponseError() Response {
	return Response{Product: "IPX800_V4", Status: "Error"}
}

func (c context) ResponseSuccess() Response {
	return Response{Product: "IPX800_V4", Status: "Success"}
}
