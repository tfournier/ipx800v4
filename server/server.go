package server

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

const indent = "    "

// New IPX server
func New(address string, apiKey interface{}, debug bool) {
	// Echo instance
	e := echo.New()

	// Hide default
	e.HideBanner = true
	e.HidePort = true

	// Middleware
	e.Use(middleware.Recover())
	e.Use(setContext())

	if debug {
		e.Use(middleware.Logger())
	}

	// Routes
	e.GET("/api/xdevices.json", api, func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := c.(*context)

			if apiKey != nil {
				if c.QueryParam("key") != fmt.Sprintf("%v", apiKey) {
					return c.JSONPretty(http.StatusOK, cc.ResponseError(), indent)
				}
			}

			return next(c)
		}
	})

	// Start server
	e.Logger.Fatal(e.Start(address))
}

func api(c echo.Context) error {
	cc := c.(*context)

	for k, v := range cc.QueryParams() {
		switch k {
		case "Get":
			switch v[0] {
			case "all":
				return getAll(cc)
			case "R":
				return getR(cc)
			}
		case "SetR":
			return setR(cc)
		case "ClearR":
			return clearR(cc)
		case "ToggleR":
			return toggleR(cc)
		}
	}

	return cc.JSONPretty(http.StatusOK, cc.ResponseError(), indent)
}
