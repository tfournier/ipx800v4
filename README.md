# [GCE Electronics](https://www.gce-electronics.com/) IPX800V4 client

[![pipeline status](https://gitlab.com/tfournier/ipx800v4/badges/master/pipeline.svg)](https://gitlab.com/tfournier/ipx800v4/-/commits/master)
[![coverage report](https://gitlab.com/tfournier/ipx800v4/badges/master/coverage.svg)](https://gitlab.com/tfournier/ipx800v4/-/commits/master)
[![go report](https://goreportcard.com/badge/gitlab.com/tfournier/ipx800v4)](https://goreportcard.com/report/gitlab.com/tfournier/ipx800v4)
