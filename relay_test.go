package ipx800v4_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tfournier/ipx800v4"
)

func TestGetRelays(t *testing.T) {
	state, err := ipx.Relays().Get(1)
	assert.Equal(t, false, state)
	assert.Nil(t, err)

	state, err = ipx.Relays().Get(0)
	assert.Equal(t, false, state)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	state, err = ipx.Relays().Get(57)
	assert.Equal(t, false, state)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	state, err = ipx800v4.New("invalid", nil).Relays().Get(1)
	assert.Error(t, err)
	assert.Equal(t, false, state)
}

func TestSetRelay(t *testing.T) {
	err := ipx.Relays().Set(0)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	err = ipx.Relays().Set(57)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	err = ipx.Relays().Set(1)
	assert.NoError(t, err)

	state, err := ipx.Relays().Get(1)
	assert.Equal(t, true, state)
	assert.Nil(t, err)

	err = ipx800v4.New("invalid", nil).Relays().Set(1)
	assert.Error(t, err)
}

func TestClearRelay(t *testing.T) {
	err := ipx.Relays().Clear(0)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	err = ipx.Relays().Clear(57)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	err = ipx.Relays().Clear(1)
	assert.NoError(t, err)

	state, err := ipx.Relays().Get(1)
	assert.Equal(t, false, state)
	assert.Nil(t, err)

	err = ipx800v4.New("invalid", nil).Relays().Clear(1)
	assert.Error(t, err)
}

func TestToggleRelay(t *testing.T) {
	err := ipx.Relays().Toggle(0)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	err = ipx.Relays().Toggle(57)
	assert.EqualError(t, err, ipx800v4.ErrInvalidRelayID.Error())

	beforeState, err := ipx.Relays().Get(1)
	assert.Nil(t, err)

	err = ipx.Relays().Toggle(1)
	assert.NoError(t, err)

	afterState, err := ipx.Relays().Get(1)
	assert.Equal(t, !beforeState, afterState)
	assert.Nil(t, err)

	err = ipx800v4.New("invalid", nil).Relays().Toggle(1)
	assert.Error(t, err)
}
