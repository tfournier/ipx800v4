package ipx800v4_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/tfournier/ipx800v4"
	"gitlab.com/tfournier/ipx800v4/server"
	"testing"
	"time"
)

const (
	address = "127.0.0.1:8080"
	apiKey  = "apikey"
)

var ipx *ipx800v4.IPX800V4

func init() {
	go server.New(address, apiKey, false)
	time.Sleep(time.Second * 5)
	ipx = ipx800v4.New(address, apiKey)
	ipx.SetDebug()
}

func TestRequest(t *testing.T) {
	res, err := ipx.Request(ipx800v4.Commands{"Fake": "Command"})
	assert.Error(t, err)
	assert.Nil(t, res)

	res, err = ipx.Request(ipx800v4.Commands{"Invalid Command": nil})
	assert.Error(t, err)
	assert.Nil(t, res)

	res, err = ipx800v4.New("\\", nil).Request(ipx800v4.Commands{"Fake": "Command"})
	assert.Error(t, err)
	assert.Nil(t, res)
}
