package ipx800v4

import (
	"fmt"
	"strings"
)

// Commands is API URL arguments
type Commands map[string]interface{}

// ToString return commands to query params
func (cmds Commands) ToString() string {
	cmd := make([]string, len(cmds))

	for key, value := range cmds {
		cmd = append(cmd, fmt.Sprintf("%s=%v", key, value))
	}

	return strings.Join(cmd, "&")
}
