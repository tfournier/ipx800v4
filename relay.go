package ipx800v4

import "fmt"

var ErrInvalidRelayID = fmt.Errorf("invalid relay id")

type Relays struct {
	IPX800V4
}

func (r Relays) List() (map[int]bool, error) {
	res, err := r.Request(Commands{"Get": "R"})
	if err != nil {
		return nil, err
	}

	return map[int]bool{
		1:  *res.R1 == 1,
		2:  *res.R2 == 1,
		3:  *res.R3 == 1,
		4:  *res.R4 == 1,
		5:  *res.R5 == 1,
		6:  *res.R6 == 1,
		7:  *res.R7 == 1,
		8:  *res.R8 == 1,
		9:  *res.R9 == 1,
		10: *res.R10 == 1,
		11: *res.R11 == 1,
		12: *res.R12 == 1,
		13: *res.R13 == 1,
		14: *res.R14 == 1,
		15: *res.R15 == 1,
		16: *res.R16 == 1,
		17: *res.R17 == 1,
		18: *res.R18 == 1,
		19: *res.R19 == 1,
		20: *res.R20 == 1,
		21: *res.R21 == 1,
		22: *res.R22 == 1,
		23: *res.R23 == 1,
		24: *res.R24 == 1,
		25: *res.R25 == 1,
		26: *res.R26 == 1,
		27: *res.R27 == 1,
		28: *res.R28 == 1,
		29: *res.R29 == 1,
		30: *res.R30 == 1,
		31: *res.R31 == 1,
		32: *res.R32 == 1,
		33: *res.R33 == 1,
		34: *res.R34 == 1,
		35: *res.R35 == 1,
		36: *res.R36 == 1,
		37: *res.R37 == 1,
		38: *res.R38 == 1,
		39: *res.R39 == 1,
		40: *res.R40 == 1,
		41: *res.R41 == 1,
		42: *res.R42 == 1,
		43: *res.R43 == 1,
		44: *res.R44 == 1,
		45: *res.R45 == 1,
		46: *res.R46 == 1,
		47: *res.R47 == 1,
		48: *res.R48 == 1,
		49: *res.R49 == 1,
		50: *res.R50 == 1,
		51: *res.R51 == 1,
		52: *res.R52 == 1,
		53: *res.R53 == 1,
		54: *res.R54 == 1,
		55: *res.R55 == 1,
		56: *res.R56 == 1,
	}, nil
}

func (r Relays) Get(id int) (bool, error) {
	if !(1 <= id && id <= 56) {
		return false, ErrInvalidRelayID
	}

	relays, err := r.List()
	if err != nil {
		return false, err
	}

	return relays[id], nil
}

func (r Relays) Set(id int) error {
	if !(1 <= id && id <= 56) {
		return ErrInvalidRelayID
	}

	_, err := r.Request(Commands{"SetR": id})
	if err != nil {
		return err
	}

	return nil
}

func (r Relays) Clear(id int) error {
	if !(1 <= id && id <= 56) {
		return ErrInvalidRelayID
	}

	_, err := r.Request(Commands{"ClearR": id})
	if err != nil {
		return err
	}

	return nil
}

func (r Relays) Toggle(id int) error {
	if !(1 <= id && id <= 56) {
		return ErrInvalidRelayID
	}

	_, err := r.Request(Commands{"ToggleR": id})
	if err != nil {
		return err
	}

	return nil
}
