package ipx800v4_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tfournier/ipx800v4"
)

func TestGetAll(t *testing.T) {
	res, err := ipx.All().Get()
	assert.Nil(t, err)
	assert.Equal(t, "IPX800_V4", res.Product)
	assert.Equal(t, "Success", res.Status)

	res, err = ipx800v4.New("invalid", nil).All().Get()
	assert.Error(t, err)
	assert.Nil(t, res)
}
